# PRW.jl Documentation

`PRW` is a package for reading the far-field data from binary `PRW` files,
as exported from some radiometric goniometers.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/PRW.jl
```

## Features

* Importing `PRW` files
* Utilities added for switching coordinate convention (`PRW` files use positive and negative polar angles)
* Utilities to interpolate far-field onto a Cartesian grid

## TODOs

* Polar->spherical?
* Example usage
