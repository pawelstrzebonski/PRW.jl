# Example Usage

We have an example far-field saved in the file `ff.prw`. We read it's
contents:

```@example ff
import PRW

(θ, ϕ, ff)=PRW.readPRW("ff.prw")
# Change the convention to have positive polar angles
(θ, ϕ, ff)=PRW.anglenormalize(θ, ϕ, ff)
nothing # hide
```

We could try plotting the far-field as a polar plot, but that can be
tricky, and for some applications we want or need far-fields on a
Cartesian grid. As such, we'll use the
[`KNNRegression`](https://gitlab.com/pawelstrzebonski/KNNRegression.jl)
package to interpolate onto a Cartesian grid:

```@example ff
import KNNRegression
# Convert the far-field data into Cartesian coordinates
θ, ϕ=vec(θ*ones(length(ϕ))'), vec(ones(length(θ))*ϕ')
x, y=@. θ*cos(ϕ), @. θ*sin(ϕ)
points=[x y]'

# Create the interpolator function and interpolate 
f=KNNRegression.knnregressor(points, ff[:])

# Create the Cartesian grid and interpolate the far-field onto the Cartesian grid
xrng=-pi/4:0.005:pi/4
yrng=-pi/4:0.005:pi/4
x, y=vec(xrng*ones(length(yrng))'), vec(ones(length(xrng))*yrng')
points=[x y]'
ffcart=reshape(f(points), length(xrng), length(yrng))
nothing # hide
```

Now we plot using the
[`Plots`](https://github.com/JuliaPlots/Plots.jl)
package:

```@example ff
import Plots: heatmap
import Plots: savefig # hide
heatmap(xrng, yrng, ffcart)
savefig("ff.png"); nothing # hide
```

![Far-field](ff.png)
