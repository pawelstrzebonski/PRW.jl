# file.jl

The binary `PRW` file is structured as such:

* Number of sampling points along azimuthal angle (`nϕ::Int16`)
* Number of sampling points along polar angle (`nθ::Int16`)
* Array of the polar angles (`θ::Array{Float32, 1}` of length `nθ`)
* Array of the intensity values (`pts::Array{Float32, 1}` of length `nθ*nϕ` that is reshaped to `pts::Array{Float32, 2}` of size `(nθ, nϕ)`)

The azimuthal angle is swept 180 degrees (so `ϕ=LinRange(0, π-(π/nϕ), nϕ)`,
and the polar angles extend both in positive and negative directions to
give full 360 degree scan.

```@autodocs
Modules = [PRW]
Pages   = ["file.jl"]
```
