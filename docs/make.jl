using Documenter
import PRW

makedocs(
    sitename = "PRW.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "PRW.jl"),
    pages = [
        "Home" => "index.md",
        "Example Usage" =>"example.md",
        "src/" => ["file.jl" => "file.md", "utils.jl" => "utils.md"],
        "test/" => ["file.jl" => "file_test.md", "utils.jl" => "utils_test.md"],
    ],
)
