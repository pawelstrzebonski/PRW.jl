# PRW

## About

`PRW.jl` is a package for reading the far-field data from binary `PRW` files,
as exported from some radiometric goniometers.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/PRW.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for PRW.jl](https://pawelstrzebonski.gitlab.io/PRW.jl/).
