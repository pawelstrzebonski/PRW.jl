"""
    readPRW(filename)

Reads `.prw` binary files of far-field images from the radiometric
goniometer. Returns θ (polar angle), ϕ (azimuthal angle), and the intensity
array ff. Angles are in radians.
"""
function readPRW(filename)
    open(filename) do s
        nϕ = read(s, Int16)
        nθ = read(s, Int16)
        θ = Float32.(reinterpret(Float32, read(s, 4 * nθ)))
        @assert length(θ) == nθ
        pts = reshape(Float32.(reinterpret(Float32, read(s, 4 * nθ * nϕ))), (nθ, nϕ))
        @assert size(pts) == (nθ, nϕ)
        ϕ = LinRange(0, π - (π / nϕ), nϕ)
        return θ, [ϕ...], pts
    end
end
