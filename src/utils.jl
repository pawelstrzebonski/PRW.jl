"""
    anglenormalize(θ, ϕ, ff)->(θ, ϕ, ff)

The data from `.prw` files is a bit odd as the goniometer only rotates
π radians and takes slices in both the negative and positive polar angle
directions. This renormalizes the inputs to have non-negative polar angles.
"""
function anglenormalize(θ::AbstractVector, ϕ::AbstractVector, ff::AbstractMatrix)
    θ[Int(length(θ) / 2 + 0.5):end],
    [ϕ; π .+ ϕ],
    hcat(ff[Int(length(θ) / 2 + 0.5):-1:1, :], ff[Int(length(θ) / 2 + 0.5):end, :])
end
