# Open a test file and check for reasonableness of output

filename="ff.prw"
@test (PRW.readPRW(filename); true)
(θ, ϕ, ff)=PRW.readPRW(filename)
@test size(ff)==(length(θ), length(ϕ))
@test -pi/2<=minimum(θ)
@test pi/2>=maximum(θ)
@test 0<=minimum(ϕ)
@test pi>=maximum(ϕ)
