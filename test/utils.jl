# Read an example far-field
filename="ff.prw"
(θ, ϕ, ff)=PRW.readPRW(filename)

@test (PRW.anglenormalize(θ, ϕ, ff); true)
(θ, ϕ, ff)=PRW.anglenormalize(θ, ϕ, ff)
@test size(ff)==(length(θ), length(ϕ))
@test 0<=minimum(θ)
@test pi/2>=maximum(θ)
@test 0<=minimum(ϕ)
@test 2*pi>=maximum(ϕ)
